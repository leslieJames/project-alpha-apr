from django.forms import ModelForm
from tasks.models import Task
from django import forms


class CreateTaskForm(ModelForm):
    class Meta:
        model = Task
        fields = [
            "name",
            "start_date",
            "due_date",
            "assignee",
        ]
        widgets = {
            'start_date': forms.DateInput(attrs={
                'type': 'date'}),
            'due_date': forms.DateInput(attrs={
                'type': 'date'}),
        }


class EditTaskForm(ModelForm):
    class Meta:
        model = Task
        fields = [
            "name",
            "start_date",
            "due_date",
            'is_completed',
           
        ]
        widgets = {
            'start_date': forms.DateInput(attrs={
                'type': 'date'}),
            'due_date': forms.DateInput(attrs={
                'type': 'date'}),
        }
