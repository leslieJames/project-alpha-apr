
from django.shortcuts import render
from tasks.models import Task
from projects.models import Project
from django.db.models import Count
from django.utils import timezone
from django.contrib.auth.decorators import login_required
import plotly.graph_objs as go
import plotly.offline as pyo



@login_required
def dashboard(request):
    user = request.user  
    
   
    total_projects = Project.objects.filter(owner=user).count()
    completed_projects = Project.objects.filter(owner=user, is_completed=True).count()
    
    # need to get current projects or ones that are created but not complete
    ongoing_projects = total_projects - completed_projects
    
    today = timezone.now().date()
    
    completed_tasks = Task.objects.filter(project__owner=user, is_completed=True).extra(select={'date': 'DATE(due_date)'}).values('date').annotate(count=Count('id')).order_by('date')
    # learned about .extra it adds more options for the query
    # select={'date': 'DATE(due_date)'} this gets the due date for the task
    # .values('date') this converts the set into a list of dictionaries and each dictionary contains only the due date of the task
    # .annotate(count=Count('id'))This part groups the tasks by their due date and counts how many tasks fall on each date.
    upcoming_tasks = Task.objects.filter(project__owner=user, due_date__gte=today, is_completed=False).order_by('due_date')[:5]

    dates = [task['date'] for task in completed_tasks]
    counts = [task['count'] for task in completed_tasks]

    trace = go.Scatter(x=dates, y=counts, mode='lines+markers', name='Completed Tasks')
    # go scatter is a plotly function
    layout = go.Layout(title='Tasks Completed', xaxis=dict(title='Date'), yaxis=dict(title='Number of Tasks'))
    fig = go.Figure(data=[trace], layout=layout)
    chart = pyo.plot(fig, output_type='div', include_plotlyjs=False)

    context = {
        'completed_tasks': completed_tasks,
        'total_projects': total_projects,
        'completed_projects': completed_projects,
        'ongoing_projects': ongoing_projects,
        'upcoming_tasks': upcoming_tasks,
         'chart': chart,
    }
    return render(request, 'analytics/dashboard.html', context)



