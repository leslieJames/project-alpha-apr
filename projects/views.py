from django.shortcuts import render, get_object_or_404, redirect
from projects.models import Project
import time 
from django.contrib.auth.decorators import login_required
from projects.forms import CreateProjectForm, EditProjectForm
from tasks.models import Task

# Create your views here.


@login_required
def list_projects(request):
    projects = Project.objects.filter(owner=request.user)
    context = {
        "list_projects": projects,
    }
    return render(request, "projects/list_projects.html", context)


@login_required
def show_project(request, id):
    project = get_object_or_404(Project, id=id)
    tasks = project.tasks.all()
    context = {
        "tasks": tasks,
        "list_projects": project,
    }

    return render(request, "projects/show_project.html", context)


@login_required
def create_project(request):
    if request.method == "POST":
        form = CreateProjectForm(request.POST)
        if form.is_valid():
            project = form.save(False)
            project.owner = request.user
            project.save()
            
            return redirect("list_projects")        
    else:
        form = CreateProjectForm()
    context = {
        "form": form,
    }

    return render(request, "projects/create_project.html", context)


@login_required
def edit_project(request, id):
    project = get_object_or_404(Project, id=id)
    if request.method == "POST":
        form = EditProjectForm(request.POST, instance=project)
        if form.is_valid():
            form.save()
            time.sleep(5)
            return redirect("list_projects")
    else:
        form = EditProjectForm(instance=project)

    context = {
        'form': form,
        'project': project,
    }
    return render(request, "projects/edit_project.html", context)


@login_required
def delete_project(request, id):
    project = get_object_or_404(Project, id=id)
    project.delete()
    return redirect("list_projects")


@login_required
def toggle_completion_view(request, task_id):
    task = get_object_or_404(Task, id=task_id)
    task.is_completed = not task.is_completed
    task.save()
    return redirect('show_my_tasks')
