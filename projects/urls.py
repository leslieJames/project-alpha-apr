from django.urls import path
from projects.views import list_projects, show_project, create_project
from projects.views import edit_project
from projects.views import delete_project, toggle_completion_view


urlpatterns = [
    path("", list_projects, name="list_projects"),
    path("<int:id>/", show_project, name="show_project"),
    path("create/", create_project, name="create_project"),
    path('<int:id>/edit/', edit_project, name='edit_project'),
    path('delete/<int:id>/', delete_project, name='delete_project'),
    path('toggle-completion/<int:task_id>/', toggle_completion_view, name='toggle_completion') 
]
