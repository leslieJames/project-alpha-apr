from django.urls import path
from accounts.views import user_login, user_logout, signup, landing_page


urlpatterns = [
    path("login/", user_login, name="login"),
    path("logout/", user_logout, name="logout"),
    path("signup/", signup, name="signup"),
    path("landingPage/", landing_page, name="landing_page"),

]
